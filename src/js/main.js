(function(window){
  'use strict';
  var 
    w = window
   ,d = w.document;

  function noLinks(e) {
    var tag = e.target;
    if (tag.tagName.toUpperCase() === 'A' && tag.getAttribute('href').trim() === '#') {
      e.preventDefault();
    }
  }

  d.addEventListener('click', noLinks, false);
}(window));

var bp = {
  medium: 1200,
  small: 1023,
  xsmall: 767,
  xxsmall: 480
};

var headerPromo = $('.promo-block');

headerPromo.slick({
  // dots: true,
  infinite: true,
  speed: 500,
  adaptiveHeight: true,
  autoplay: true,
  autoplaySpeed: 3000,
  slidesToShow: 1,
  slidesToScroll: 1,
    responsive: [
    {
      breakpoint: 10000,
      // settings: "unslick"
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }]
});

var features = $('.features');

// if ($( document ).width() < bp.xsmall) {
  features.slick({
    dots: true,
    slidesToShow: 1,
    mobileFirst: true,
    autoplay: true,
    autoplaySpeed: 2000,
    responsive: [
      // {
      //   breakpoint: 1024,
      //   settings: {
      //     slidesToShow: 3,
      //     slidesToScroll: 3,
      //     infinite: true,
      //     dots: true
      //   }
      // },
      // {
      //   breakpoint: 600,
      //   settings: {
      //     slidesToShow: 2,
      //     slidesToScroll: 2
      //   }
      // },
      // {
      //   breakpoint: bp.xxsmall,
      //   settings: {
      //     slidesToShow: 1,
      //     slidesToScroll: 1
      //   }
      // },
      {
        breakpoint: bp.xsmall,
        settings: 'unslick'
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  });
// };

// $(window).resize(function() {
//   if ($( document ).width() < bp.xsmall) {
//     features.slick({
//       dots: true,
//       slidesToShow: 1
//     });
//   } else {
//     features.slick('unslick');
//   }
// });

// features.slick({
//   dots: true,
//   slidesToShow: 1
// });

var calcForm = $('.calc-form');
var sumField = calcForm.find('.sum');

calcForm.find('input').keypress(function(e) {
  var code = e.keyCode;
  if ( !(code >= 48 && code <=57 || code === 46) ) {
    return false;
  }
});

$('a[data-scroll-to]').click(function(e) {
  e.preventDefault();

  var offset = $( '.' + $(this).attr('data-scroll-to') ).offset().top;

  $('html, body').animate({
    scrollTop: offset
  }, 600);
});

var calculate = function () {
  var $calculator = $('.calculator');
  var $sum = $('.ceiling-sum .sum');
  var sum = 0;
  var ceilingMaterial = $calculator
                          .find('.ceiling-material option:selected')
                          .attr('data-price');
  ceilingMaterial = parseFloat(ceilingMaterial);


  var ceilingArea = $calculator.find('.ceiling-area input').val();

  if (ceilingArea === '') {
    ceilingArea = 0;
  } else {
    ceilingArea = parseFloat(ceilingArea);
  }
  ceilingArea *= ceilingMaterial;

  var ceilingAngularAmout = $calculator
                            .find('.ceiling-angular-amount input')
                            .val();

  ceilingAngularAmout = ceilingAngularAmout === '' ? 0 : parseInt(ceilingAngularAmout);
  ceilingAngularAmout *= parseInt($calculator.find('.ceiling-angular-amount input').attr('data-price'));

  var ceilingLightAmount = $calculator.find('.ceiling-lights-amount input').val();
  ceilingLightAmount = ceilingLightAmount === '' ? 0 : parseInt(ceilingLightAmount);
  ceilingLightAmount *= parseInt($calculator.find('.ceiling-lights-size input[type="radio"]:checked').attr('data-price'));

  var ceilingAngularity = $calculator
                            .find('.ceiling-angularity input')
                            .val();
  ceilingAngularity = ceilingAngularity === '' ? 0 : parseInt(ceilingAngularity);
  ceilingAngularity *= parseInt($calculator.find('.ceiling-angularity input').attr('data-price'));

  sum = ceilingArea + ceilingAngularAmout + ceilingLightAmount + ceilingAngularity;

  console.log('changed!!');

  $sum.text(sum);
};

calculate();

calcForm.find('input[type="radio"], input[type="checkbox"], select').change(function(e) {
  calculate();
});

calcForm.find('input[type="text"]').keyup(function(e) {
  calculate();
});


// calcForm.find('input[type=text]').keyup(function(e) {
//   var sum = 0;

//   $('.calc-form .form-control:not(:last-of-type)').each(function(i, el) {
//     var input = $(el).find('input').val();
//     console.log(input)

//     // if ($(el).hasClass('radio-block')) {
//     //   alert();
//     //   return;
//     // }

//     if (input === '') {
//       input = 0;
//     } else {
//       input = parseFloat(input);
//     }
//     console.log(input)

//     sum += parseFloat(input * parseInt($('input').attr('data-price')) );
//     // console.log(parseFloat(input.val()), input.attr('data-price'));
//     sumField.text(sum);
//   });
// });


// $(' button').click(function(e) {
  $(document).on('click', '.close-modal', function(e) {
    e.preventDefault();

    $.magnificPopup.close();
  });

  console.log('kek');
  $('.open-feedback-form').magnificPopup({
     type: 'inline',
     closeOnBgClick: true,
     showCloseBtn: true
     // modal: true
     // focus: '#name'
  });
  $('.left-application a').magnificPopup({
     type: 'inline',
     closeOnBgClick: true,
     showCloseBtn: true
     // modal: true
     // focus: '#name'
  });
// });





$(document).ready(function() {

   lightbox.option({
    'resizeDuration': 100,
    // 'wrapAround': true,
    // 'disableScrolling': true
  })

  // $('.').magnificPopup({
  //   type: 'image',
  //   closeOnContentClick: true,
  //   mainClass: 'mfp-img-mobile',
  //   image: {
  //     verticalFit: true
  //   }
    
  // });
 
  // $('.popup-gallery').magnificPopup({
  //   delegate: 'img',
  //   type: 'image',
  //   tLoading: 'Loading image #%curr%...',
  //   mainClass: 'mfp-img-mobile',
  //   gallery: {
  //     enabled: true,
  //     navigateByImgClick: true,
  //     preload: [0,1] // Will preload 0 - before current, and 1 after the current image
  //   },
  //   image: {
  //     tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
  //     titleSrc: function(item) {
  //       return item.el.attr('title') + '<small>by Marsel Van Oosten</small>';
  //     }
  //   }
  // });
});
